﻿/// <amd-dependency path="path/to/knockout/js" />

const JSONString = json =>
    JSON.stringify(
        json,
        null,
        2
    ) 

module BlizzData {
    enum Operation {
        FetchToken,
        GetTop10,
        GetTop100,
        GetLeaderboardDataByName
    }

    export class App {
        private origin = 'us'
        private locale = 'en_US' //not used
        private token = ''
        private laderBoardsList: Array<any> = []
        public LeaderMistList = ko.observableArray<any>([])
        public LeadBoardCategoryList = ko.observableArray<any>([])
        public LeadBoardAchievementList = ko.observableArray<any>([])
        public ChartList = ko.observableArray<any>([])
        public ChartList2 = ko.observableArray<any>([])
        public TopComparisonList = ko.observableArray<any>([])
        public TopAllList
        public heroClass = 'rift-barbarian'
        public hero = { class: '', name: '', thumb: '' }
        public heroClasses = [this.hero]
        public selectHero
        public chosenHeroId
        public chosenHeroName
        public seasonList = ko.observableArray<any>([])
        public seasonSlct = ko.observable(12)
        public chart = ko.observable(0)
        public achievementRank = ko.observable(false)
        public seasonChange
        private rankMethod = 'rift-barbarian'
        
        public NavHeroes() {
            // Data set - would come from server
            var self = this
            self.heroClasses = [
                { class: 'rift-barbarian', name: 'Barbarian', thumb: 'bbm' },
                { class: 'rift-wizard', name: 'Wizard', thumb: 'wzf' },
                { class: 'rift-wd', name: 'White Doctor', thumb: 'wdm' },
                { class: 'rift-dh', name: 'Demon Hunter', thumb: 'dhm' },
                { class: 'rift-monk', name: 'Monk', thumb: 'mkm' },
                { class: 'rift-crusader', name: 'Crusader', thumb: 'crf' },
                { class: 'rift-necromancer', name: 'Necromancer', thumb: 'ncm' },
            ]

            self.chosenHeroId = ko.observable() // declare as observer, to view changes
            self.chosenHeroName = ko.observable('barbarian')

            // When value change, execute:
            self.selectHero = function (data) {
                self.chosenHeroId(data.class)
                self.chosenHeroName(data.name)
                self.GetTop100()
            }
        }

        public ListSeasons() {
            // Data set - would come from server
            var self = this
            self.seasonList = ko.observableArray(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'])
            self.seasonSlct = ko.observable(12) // declare as observer, to view changes
        }

        public GetHeroLink(playerTag : string){
            const link = 'https://us.battle.net/d3/en/profile/' + playerTag.replace("#", "-") + '/'
            window.open(link)
        }
        // method to change season rank
        public ChangeSeason() {
            var self = this
            self.seasonSlct() == undefined ? self.seasonSlct(12) : self.seasonSlct()
            self.seasonSlct() <= 10 && self.heroClass === 'rift-necromancer' ? self.seasonSlct(11) : self.seasonSlct()
            //clean lists
            self.TopComparisonList([])
            self.LeaderMistList([])
            self.LeadBoardCategoryList([])
            self.LeadBoardAchievementList([])
            
            switch (self.rankMethod) {
                case '1':
                    self.GetTopComparison()
                    self.GetTop10Data()                    
                    break
                case '2':
                    self.GetTop100()
                    self.GetTopComparison()
                    break

                default:
                    self.GetTopComparison()
                    break
            }
        }

        public ConvertTime(time: number) {
            const converted = (time / 1000) / 60
            return converted.toFixed(2)
        }

        public GetClassImg(classH: string, sex: string) {
            const concatClass = classH + sex
            switch (concatClass) {
                case 'barbarianm':
                    return 'bbm'
                case 'barbarianf':
                    return 'bbf'
                case 'wizardm':
                    return 'wzm'
                case 'wizardf':
                    return 'wzf'
                case 'witch doctorm':
                    return 'wdm'
                case 'witch doctorf':
                    return 'wdf'
                case 'demon hunterm':
                    return 'dhm'
                case 'demon hunterf':
                    return 'dhf'
                case 'monkm':
                    return 'mkm'
                case 'monkf':
                    return 'mkf'
                case 'crusaderm':
                    return 'crm'
                case 'crusaderf':
                    return 'crf'
                case 'necromancerm':
                    return 'ncm'
                case 'necromancerf':
                    return 'ncf'
                default:
                    return 'undefined'
            }

        }

        public LoadTop10() {
            return () => this.GetTop10Data()
        }

        public LoadAchievements(){
            return () => this.GetAchievements()
        }

        public GetAchievements(){
            var self = this
            self.achievementRank(true)
            self.chosenHeroId('achievement-points')
            self.GetTop100()
        }

        public GetTop10Data(){
            var self = this
            if (self.token == '') {
                self.fetchToken(() => {
                    self.GetTop10Data()
                })
                return
            }
            
            let arrPromise = []
            function getData(url) {
                let rows
                var $checkSessionServer = $.get(url, {}, (response: any) => {})  
                $checkSessionServer.then(function(data){})
                
                return $checkSessionServer
            } 

            self.TopAllList = []
            self.heroClasses.forEach(element => {    
                if(!(self.seasonSlct() <= 10 && element.class == 'rift-necromancer')){                          
                    let url = self.buildUrl(self.origin, self.locale, Operation.GetTop100).replace("{season}", self.seasonSlct().toString())
                    url = url.replace("{category}", element.class)
                    arrPromise.push(getData(url))
                }
            })
            
            Promise.all(arrPromise)
                .then(arrayResult => {
                myMethodCompleted(arrayResult)

                function myMethodCompleted(results) {
                    let value1 = results[0]
                    let value2 = results[1]                
                    // organize data
                    let count = 0
                    results.forEach(element => {
                        const finalConcat = [
                            ...results[count].row,
                            ...self.TopAllList
                        ]                    
                        count++
                        self.TopAllList = finalConcat
                    })
                }
                self.GetTop10()
            })
        }
        
        public GetTopComparison(){
            var self = this
            if (self.token == '') {
                self.fetchToken(() => {
                    self.GetTopComparison()
                })
                return
            }
            
            let arrPromise = []
            function getData(url) {
                let rows
                var $checkSessionServer = $.get(url, {}, (response: any) => {})  
                $checkSessionServer.then(function(data){
                    rows = data.row
                    const ArrTwoSelected = [rows[0], rows[99]]
                    for (const row of ArrTwoSelected) { // crop top N from array
                        self.TopComparisonList.push(self.ConvertPlayersArr(row))
                    }
                })
                
                return $checkSessionServer
            } 

            self.heroClasses.forEach(element => {    
                if(!(self.seasonSlct() <= 10 && element.class == 'rift-necromancer')){                          
                    let url = self.buildUrl(self.origin, self.locale, Operation.GetTop100).replace("{season}", self.seasonSlct().toString())
                    url = url.replace("{category}", element.class)
                    getData(url)
                }
            })            
        }

        public GetTop10(){
            var self = this            
            self.chart(0) 
            self.LeadBoardCategoryList([])
            self.LeadBoardAchievementList([])
            const auxArr = []
            self.TopAllList.forEach(element => {
                if (element != null) {
                    auxArr.push(self.ConvertPlayersArr(element))
                }
            })
            function compareTier(a,b) {
                if (a.tier < b.tier)
                    return 1
                if (a.tier > b.tier)
                    return -1
                if (a.tier == b.tier){
                    if (a.riftTime < b.riftTime)
                        return -1
                    if (a.riftTime > b.riftTime)
                        return 1
                }
                return 0
            }
            function compareTime(a,b) {
                if (a.tier == b.tier){
                    if (a.riftTime < b.riftTime)
                        return -1
                    if (a.riftTime > b.riftTime)
                        return 1
                }
                return 0
            }
            self.LeaderMistList(auxArr.sort(compareTier).slice(0, 10))
            self.rankMethod = '1'
            
        }

        public GetChartData(list : any){
            var self = this
            // connect info: insidence of class on top 100 / label classes / quantity
            self.ChartList([])
            self.ChartList2([])
            const auxArr = []
            list.forEach(element => {
                if (element != null) {
                    auxArr.push({classH:element.classH, lvl:element.paragLvl,rank:element.rank})
                }
            })
            
            self.heroClasses.forEach(element => {
                var groupedClass = auxArr.filter(isHeroClass)
                let calcMedParag = 0
                let calcMedRank = 0
                groupedClass.forEach(h => {
                    calcMedParag += h.lvl    
                    calcMedRank += h.rank    
                })
                self.ChartList().push({ class: element.name, qnt: groupedClass.length, lvl: calcMedParag / groupedClass.length, rank: calcMedRank / groupedClass.length})
                
                function isHeroClass(hero) {
                    if(typeof hero.classH === "string"){
                        if(hero.classH.toString() == "witch doctor") 
                            hero.classH = "White Doctor"    
                        return hero.classH.replace(/\b\w/g, function(l){ return l.toUpperCase() }) === element.name
                    }
                }
            })           
            self.GPieChart()
            self.RankPieChart()
            self.ChartRank()
            
        }

        private GPieChart(){
            var self = this
            self.chart(1)  
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Classe')
            data.addColumn('number', 'Quantidade')
            self.ChartList().forEach(element => {
                data.addRows([
                    [element.class.toString(), element.qnt]
                ])
            })

            var options = {
                tooltip: {isHtml: true},
                titlePosition: 'none',
                legend: 'none',
                backgroundColor: 'transparent',
                colors: ['#730f0d', '#9159ae', '#5fb54e', '#626065', '#996e41', '#c9b438', '#2ba09f'], // nec: #2ba09f, cru:#c9b438, wiz: #9159ae, whit:##5fb54e, dh:#626065, monk: #996e41, barb:#730f0d
                is3D: true
            }

            var chart = new google.visualization.PieChart(document.getElementById('piechart'))

            chart.draw(data, options)
        }   
        
        private RankPieChart(){
            var self = this
            const auxArr = []
            self.ChartList().forEach(element => {
                auxArr.push([element.class.toString(), element.lvl, element.rank])
            })
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Classe')
            data.addColumn('number', 'Average Rank')
            self.ChartList().forEach(element => {
                data.addRows([
                    [element.class.toString(), element.rank]
                ])
            })
    
            var options = {
                tooltip: {isHtml: true},
                titlePosition: 'none',
                legend: 'none',
                bar: {groupWidth: '95%'},
                vAxis: { gridlines: { count: 4 } },
                backgroundColor: 'transparent',
                colors: ['#c9b438'],
                bars: 'horizontal'
            }
    
            var chart = new google.charts.Bar(document.getElementById('rankchart'));
    
            chart.draw(data, google.charts.Bar.convertOptions(options));
        }  

        public ChartRank() {
            var self = this
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Classe')
            data.addColumn('number', 'Paragon Average')
            self.ChartList().forEach(element => {
                data.addRows([
                    [element.class.toString(), element.lvl]
                ])
            })
    
            var options = {
                tooltip: {isHtml: true},
                titlePosition: 'none',
                legend: 'none',
                bar: {groupWidth: '95%'},
                hAxis: { gridlines: { count: 5 } },
                backgroundColor: 'transparent',
                colors: ['#2ba09f'],
                bars: 'horizontal'
            }
    
            var chart = new google.charts.Bar(document.getElementById('paragonchart'));
    
            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
        

        public LoadTop100() {
            return () => this.GetTop100()
        }

        public GetTop100() {
            var self = this
            self.LeaderMistList([])
            self.chosenHeroId() == undefined ? self.chosenHeroId('rift-barbarian') : self.chosenHeroId()
            self.LeadBoardCategoryList([])
            self.LeadBoardAchievementList([])
            if (self.token == '') {
                self.fetchToken(() => {
                    self.GetTop100()
                })
                return
            }
            // build request url
            self.rankMethod = '2'
            let url = self.buildUrl(self.origin, self.locale, Operation.GetTop100).replace("{season}", self.seasonSlct().toString())
            url = url.replace("{category}", self.chosenHeroId())
            $.get(url, {}, (response: any) => {
                const rows = response.row
                // organize data
                for (const row of rows.slice(0, 100)) { // crop top N from array                    
                    if (response.achievement_points) {
                        self.LeadBoardAchievementList.push(self.ConvertPlayersArr(row))
                    }else{
                        self.LeadBoardCategoryList.push(self.ConvertPlayersArr(row)) // fill the leadboard arr
                    }
                }
                if (response.achievement_points) {
                    self.GetChartData(self.LeadBoardAchievementList())
                }else{
                    self.chart(0)              
                }
            })            
        }

        public ConvertPlayersArr(row){
            var self = this
            const {
                player, // player basic info
                order, // player order on rank
                rank // player type of rank 'ex: achievments, riftLvl
            } = self.getInfoFromRow(row)

            const dataPlayer = player.data

            const playerFormated = dataPlayer.map(dataP => ({ // format player data to id / value
                id: dataP.id,
                value: self.getPlayerDataValue(dataP)
            }))

            const rankFormated = rank.map(rankData => ({ // format rank data to id / value
                id: rankData.id,
                value: self.getPlayerDataValue(rankData)
            }))

            const finalArray = [
                ...playerFormated,
                ...rankFormated
            ]
            const playerObj = self.MapInfosPlayer(finalArray)

            return playerObj
        }

        public TransferInfo(infos, data) { // receive a list of parameters (infos) and convert data into array
            return infos.reduce((acc, prop, i) => {
                const value = data[i] ? data[i].value : undefined
                acc[prop] = (value) ? value : "empty"

                return acc
            }, {})
        }

        public MapInfosPlayer(data) {
            const infos = [
                ['HeroBattleTag', 'name'],
                ['GameAccount', 'account'],
                ['HeroClass', 'classH'],
                ['HeroGender', 'sex'],
                ['HeroLevel', 'lvl'],
                ['ParagonLevel', 'paragLvl'],
                ['HeroClanTag', 'clanTag'],
                ['ClanName', 'clanName'],
                ['Rank', 'rank'],
                ['AchievementPoints', 'achievementPoints'],
                ['RiftLevel', 'tier'],
                ['RiftTime', 'riftTime'],
                ['CompletedTime', 'completedTime'],
                ['HeroId', 'id'],
                ['BattleTag', 'battleTag']
            ]

            return this.MapInfos(infos, data)
        }

        public MapInfos(infos, data) { // receive a list of parameters (infos) and convert data into array
            return infos.reduce((acc, props, i) => {
                const [originalProp, newProp] = props
                const obj = data.find(obj => obj.id === originalProp)

                acc[newProp] = obj ? obj.value : '-'

                return acc
            }, {})
        }

        private getInfoFromRow(row) {
            const { order, data } = row

            return { // format object 
                player: this.getPlayerFromRow(row),
                order,
                rank: data
            }
        }

        private getPlayerFromRow(row) { // get player from 'X' row position
            return row.player[0]
        }

        private getPlayerDataValue(dataPlayer) {
            const valueKeys = Object.keys(dataPlayer)
            const valueKeysNotId = valueKeys.filter(key => key !== 'id')
            const valueKey = valueKeysNotId[0]

            return dataPlayer[valueKey]
        }

        private fetchToken(callBack: Function = null): void {
            var url = this.buildUrl()
            $.get(url, {}, (data: any) => {
                this.token = data.access_token
                if (callBack != null) {
                    callBack()
                }
            })

        }

        private buildUrl(origin: string = 'us', locale: string = 'en', op: Operation = Operation.FetchToken) {
            var base = 'https://' + origin + '.api.battle.net/'
            op.toString()
            switch (op) {
                case Operation.FetchToken:
                    // token helper for Auth 
                    base = 'https://frontendhelperbeeye.azurewebsites.net/Tokens.asmx/GetToken'
                    break
                case Operation.GetTop100:
                    base = base + "data/d3/season/{season}/leaderboard/{category}?access_token=" + this.token
                    break
                case Operation.GetLeaderboardDataByName:
                    base = base + "data/d3/season/{season}/leaderboard/{name}?access_token=" + this.token
                    break

                default:
            }
            return base
        }

    }
}